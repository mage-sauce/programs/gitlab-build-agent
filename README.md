# Gitlab docker-compose build agent

Simple repo to run a Gitlab build agent in docker-compose
Also see LICENSE file

## Instructions
* `apt install ansible git`
* `git clone git@gitlab.com:mage-sauce/programs/gitlab-build-agent.git /opt/gitlab-build-agent`
* `make init`
* `nano config.toml` (i.e. `nano /opt/gitlab-build-agent-data/gitlab-runner-config/config.toml`) see below
* `make start`

## config.toml
### Settings 
* `environment = ["DOCKER_HOST=unix:///var/run/docker.sock"]`
* `volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]`

### Example
```
concurrent = 1
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "!!!REDACTED!!!"
  url = "https://gitlab.com/"
  token = "!!!REDACTED!!!"
  executor = "docker"
  environment = ["DOCKER_HOST=unix:///var/run/docker.sock"]
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "ubuntu:latest"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]
    shm_size = 0
```

## TODO
* Cron to auto pull the base image and restart
