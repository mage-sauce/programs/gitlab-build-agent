pull:
	docker-compose pull

init:
	cp gitlab-runner.env-dist gitlab-runner.env
	ansible-playbook ansible-playbook.yml
	make pull
	@read -p "REGISTRATION_TOKEN : " REGISTRATION_TOKEN \
	&& docker-compose run --rm gitlab-runner register --url https://gitlab.com/ --registration-token "$${REGISTRATION_TOKEN}"

stop:
	docker-compose down --volumes

start:
	docker-compose up -d

shell:
	docker-compose exec gitlab-runner /bin/bash
